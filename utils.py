

class ByteStr:
    def __init__(self, part: bytes):
        self.part = part

    def __str__(self) -> str:
        return ''.join(map(chr, self.part)).rstrip('\x00')




