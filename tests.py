import unittest
import datetime
from typing import Any

import headers


class DosHeaderTests(unittest.TestCase):

    @classmethod
    def setUpClass(cls) -> None:
        with open('SumatraPDF.exe', 'rb') as f:
            dump = f.read()
            cls.x64_dos_header = headers.DOSHeader(dump)
            cls.x64_pe_header = headers.PEHeader(dump, cls.x64_dos_header.pe_offset())
        with open('7z1902.exe', 'rb') as f:
            dump = f.read()
            cls.x32_dos_header = headers.DOSHeader(dump)
            cls.x32_pe_header = headers.PEHeader(dump, cls.x32_dos_header.pe_offset())
        super().setUpClass()


    def test_signature(self):
        self.assertEquals('MZ', self.x32_dos_header.signature())

    def test_e_cblp_i386(self):
        self.assertEqual(144, self.x32_dos_header.e_cblp())

    def test_e_crlc_i386(self):
        self.assertEqual(0, self.x32_dos_header.e_crlc())

    def test_e_cparhdr_i386(self):
        self.assertEqual(4, self.x32_dos_header.e_cparhdr())

    def test_e_minalloc_i386(self):
        self.assertEqual(0, self.x32_dos_header.e_minalloc())

    def test_e_maxalloc_i386(self):
        self.assertEqual(65535, self.x32_dos_header.e_maxalloc())

    def test_e_cp_i386(self):
        self.assertEqual(3, self.x32_dos_header.e_cp())

    def test_e_ss(self):
        self.assertEqual(0, self.x32_dos_header.e_ss())

    def test_e_sp(self):
        self.assertEqual(184, self.x32_dos_header.e_sp())

    def test_e_csum(self):
        self.assertEqual(0, self.x32_dos_header.e_csum())

    def test_e_ip(self):
        self.assertEqual(0, self.x32_dos_header.e_ip())

    def test_e_cs(self):
        self.assertEqual(0, self.x32_dos_header.e_cs())

    def test_e_lfarlc(self):
        self.assertEqual(64, self.x32_dos_header.e_lfarlc())

    def test_e_ovno(self):
        self.assertEqual(0, self.x32_dos_header.e_ovno())

    def test_e_oemid(self):
        self.assertEqual(0, self.x32_dos_header.e_oemid())

    def test_e_oeinfo(self):
        self.assertEqual(0, self.x32_dos_header.e_oeminfo())

    def test_pe_offset(self):
        self.assertEquals(240, self.x32_dos_header.pe_offset())


class PEHeaderTests(unittest.TestCase):

    @classmethod
    def setUpClass(cls) -> None:
        with open('SumatraPDF.exe', 'rb') as f:
            dump = f.read()
            cls.x64_dos_header = headers.DOSHeader(dump)
            cls.x64_pe_header = headers.PEHeader(dump, cls.x64_dos_header.pe_offset())
        with open('7z1902.exe', 'rb') as f:
            dump = f.read()
            cls.x32_dos_header = headers.DOSHeader(dump)
            cls.x32_pe_header = headers.PEHeader(dump, cls.x32_dos_header.pe_offset())
        super().setUpClass()

    def test_machine_amd_64(self):
        self.assertEqual('IMAGE_FILE_MACHINE_AMD64', self.x64_pe_header.machine())

    def test_file_machine_i386(self):
        self.assertEqual('IMAGE_FILE_MACHINE_I386', self.x32_pe_header.machine())

    def test_number_of_sections(self):
        self.assertEqual(4, self.x32_pe_header.number_of_sections())

    def test_time_date_stamp(self):
        # 2019-09-05 18:00:00
        dt1 = datetime.datetime(2019, 9, 5, 18, 0, 0)
        self.assertEqual(dt1, self.x32_pe_header.time_date_stamp())

    def test_pointer_to_symbol_table(self):
        self.assertEqual(0, self.x32_pe_header.pointer_to_symbol_table())

    def test_number_of_symbols(self):
        self.assertEqual(0, self.x32_pe_header.number_of_symbols())

    def test_size_of_optional_header(self):
        self.assertEqual(224, self.x32_pe_header.size_of_optional_header())

    def test_characteristics(self):
        chrtr = ['IMAGE_FILE_RELOCS_STRIPPED',
                 'IMAGE_FILE_EXECUTABLE_IMAGE',
                 'IMAGE_FILE_LINE_NUMS_STRIPPED',
                 'IMAGE_FILE_LOCAL_SYMS_STRIPPED',
                 'IMAGE_FILE_LARGE_ADDRESS_AWARE',
                 'IMAGE_FILE_32BIT_MACHINE']
        self.assertEqual(chrtr, self.x32_pe_header.characteristics())


class OptionalHeaderTests(unittest.TestCase):

    @classmethod
    def setUpClass(cls) -> None:
        with open('SumatraPDF.exe', 'rb') as f:
            dump = f.read()
            cls.x64_dos_header = headers.DOSHeader(dump)
            cls.x64_pe_header = headers.PEHeader(dump, cls.x64_dos_header.pe_offset())
            cls.x64_optional_header = headers.OptionalHeader(dump, cls.x64_dos_header.pe_offset() + 24)
        with open('7z1902.exe', 'rb') as f:
            dump = f.read()
            cls.x32_dos_header = headers.DOSHeader(dump)
            cls.x32_pe_header = headers.PEHeader(dump, cls.x32_dos_header.pe_offset() + 24)
            cls.x32_optional_header = headers.OptionalHeader(dump, cls.x32_pe_header.offset)
        super().setUpClass()

    def test_magic(self):
        self.assertEqual('x32', self.x32_optional_header.magic())

    def test_address_of_entry_point(self):
        self.assertEqual('0x7314', self.x32_optional_header.address_of_entry_point())

    def test_base_of_code(self):
        self.assertEqual('0x1000', self.x32_optional_header.base_of_code())

    def test_base_of_data(self):
        self.assertEqual('0x8000', self.x32_optional_header.base_of_data())

    def test_image_base(self):
        self.assertEqual('0x400000', self.x32_optional_header.image_base())

    def test_section_alignment(self):
        self.assertEqual('0x1000', self.x32_optional_header.section_alignment())

