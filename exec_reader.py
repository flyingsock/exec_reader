from sys import exit, argv
import headers

if __name__ == '__main__':
    if '-h' in argv or '--help' in argv or len(argv) < 3:
        print('usage: python exec_reader.py [-a[all]] <path to file.exe>')
        exit(1)

    if '-a' in argv:
        with open(argv[2], 'rb') as f:
            dump = f.read()

            dos_header = headers.DOSHeader(dump)
            if headers.is_valid_dos_header(dos_header):
                print('DOS Header: Valid')
            else:
                print('DOS Header: Invalid')
                exit(-1)
            print('       DOS HEADER:')
            print('Bytes of last page: {}'.format(dos_header.e_cblp()))
            print('Pages in file: {}'.format(dos_header.e_cp()))
            print('Number of relocations: {}'.format(dos_header.e_crlc()))
            print('msdos header size(in paragraphs): {}'.format(dos_header.e_cparhdr()))
            print('minimum paragraphs: {}'.format(dos_header.e_minalloc()))
            print('maximum paragraphs: {}'.format(dos_header.e_maxalloc()))
            print('stack-segment module: {}'.format(dos_header.e_ss()))
            print('SP register: {}'.format(dos_header.e_sp()))
            print('checksum: {}'.format(dos_header.e_csum()))
            print('IP register: {}'.format(dos_header.e_ip()))
            print('Initial (relative) CS value: {}'.format(dos_header.e_cs()))
            print('File address of relocation table: {}'.format(dos_header.e_lfarlc()))
            print('Overlay number: {}'.format(dos_header.e_ovno()))
            print('OEM identifier: {}'.format(dos_header.e_oemid()))
            print('OEM information: {}'.format(dos_header.e_oeminfo()))
            print('PE Offset: {}'.format(dos_header.pe_offset()))
            pe_header = headers.PEHeader(dump, dos_header.pe_offset())
            print('       PE HEADER:')
            print('Machine: {}'.format(pe_header.machine()))
            print('Number of sections: {}'.format(pe_header.number_of_sections()))
            print('Time date stamp: {}'.format(pe_header.time_date_stamp()))
            print('pointer_to_symbol_table: {}'.format(pe_header.pointer_to_symbol_table()))
            print('number_of_symbols: {}'.format(pe_header.number_of_symbols()))
            print('Size of optional headers: {} bytes'.format(pe_header.size_of_optional_header()))
            print('PE characteristics: {}'.format(pe_header.characteristics()))
            optional_header = headers.OptionalHeader(dump, dos_header.pe_offset() + 24)
            print('       OPTIONAL HEADER:')

            print('Image: {}'.format(optional_header.magic()))
            print('Major linker version: {}'.format(optional_header.major_linker_version()))
            print('Minor linker version: {}'.format(optional_header.minor_linker_version()))
            print('Address of entry point: {}'.format(optional_header.address_of_entry_point()))
            print('Base of code: {}'.format(optional_header.base_of_code()))
            print('Base of data: {}'.format(optional_header.base_of_data()))
            print('Image base: {}'.format(optional_header.image_base()))
            print('Section alignment: {}'.format(optional_header.section_alignment()))
            print('File alignment: {}'.format(optional_header.file_alignment()))
            print('Major subsystem version: {}'.format(optional_header.major_subsystem_version()))
            print('Minor subsystem version: {}'.format(optional_header.minor_subsystem_version()))
            print('Size of image: {}'.format(optional_header.size_of_image()))
            print('Size of headers: {}'.format(optional_header.size_of_headers()))
            print('Checksum: {}'.format(optional_header.checksum()))
            print('Subsystem value: {}'.format(optional_header.subsystem()))
            print('Number of rva and sizes: {}'.format(optional_header.number_of_rva_and_sizes()))

            print('{}'.format(dos_header.pe_offset()))

            data_directory = headers.DataDirectory(dump, optional_header.optional_offset,
                                                   optional_header.number_of_rva_and_sizes(), optional_header.magic())
            print('{}'.format(data_directory.image_data_directory()))
            print(optional_header.optional_offset)

            section_table = headers.SectionTable(dump, optional_header.magic(), optional_header.optional_offset,
                                                 pe_header.number_of_sections())
            print('Sections table: {}'.format(section_table.section_table()))

            print('{}'.format(dos_header.dos_header()))


