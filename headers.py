import struct
from datetime import datetime
import collections

import utils


CHARACTERISTIC_MASKS = {
    0x0001: 'IMAGE_FILE_RELOCS_STRIPPED',
    0x0002: 'IMAGE_FILE_EXECUTABLE_IMAGE',
    0x0004: 'IMAGE_FILE_LINE_NUMS_STRIPPED',
    0x0008: 'IMAGE_FILE_LOCAL_SYMS_STRIPPED',
    0x0010: 'IMAGE_FILE_AGGRESSIVE_WS_TRIM',
    0x0020: 'IMAGE_FILE_LARGE_ADDRESS_AWARE',
    0x0080: 'IMAGE_FILE_BYTES_REVERSED_LO',
    0x0100: 'IMAGE_FILE_32BIT_MACHINE',
    0x0200: 'IMAGE_FILE_DEBUG_STRIPPED',
    0x0400: 'IMAGE_FILE_REMOVABLE_RUN_FROM_SWAP',
    0x0800: 'IMAGE_FILE_NET_RUN_FROM_SWAP',
    0x1000: 'IMAGE_FILE_SYSTEM',
    0x2000: 'IMAGE_FILE_DLL',
    0x4000: 'IMAGE_FILE_UPSYSTEM_ONLY',
    0x8000: 'IMAGE_FILE_BYTES_REVERSED_HI'
}
SUBSYSTEM_VALUE = {
    0: 'IMAGE_SUBSYSTEM_UNKNOWN',
    1: 'IMAGE_SUBSYSTEM_NATIVE',
    2: 'IMAGE_SUBSYSTEM_WINDOWS_GUI',
    3: 'IMAGE_SUBSYSTEM_WINDOWS_CUI',
    5: 'IMAGE_SUBSYSTEM_OS2_CUI',
    7: 'IMAGE_SUBSYSTEM_POSIX_CUI',
    8: 'IMAGE_SUBSYSTEM_NATIVE_WINDOWS',
    9: 'IMAGE_SUBSYSTEM_WINDOWS_CE_GUI',
    10: 'IMAGE_SUBSYSTEM_EFI_APPLICATION',
    11: 'IMAGE_SUBSYSTEM_EFI_BOOT_SERVICE_DRIVER',
    12: 'IMAGE_SUBSYSTEM_EFI_RUNTIME_DRIVER',
    13: 'IMAGE_SUBSYSTEM_EFI_ROM',
    14: 'IMAGE_SUBSYSTEM_XBOX',
    16: 'IMAGE_SUBSYSTEM_WINDOWS_BOOT_APPLICATION'
}
MACHINE_TYPES = {
    0x0: 'IMAGE_FILE_MACHINE_UNKNOWN',
    0x1d3: 'IMAGE_FILE_MACHINE_AM33',
    0x8664: 'IMAGE_FILE_MACHINE_AMD64',
    0x1c0: 'IMAGE_FILE_MACHINE_ARM',
    0xaa64: 'IMAGE_FILE_MACHINE_ARM64',
    0x1c4: 'IMAGE_FILE_MACHINE_ARMNT',
    0xebc: 'IMAGE_FILE_MACHINE_EBC',
    0x14c: 'IMAGE_FILE_MACHINE_I386',
    0x200: 'IMAGE_FILE_MACHINE_IA64',
    0x9041: 'IMAGE_FILE_MACHINE_M32R',
    0x266: 'IMAGE_FILE_MACHINE_MIPS16',
    0x366: 'IMAGE_FILE_MACHINE_MIPSFPU',
    0x466: 'IMAGE_FILE_MACHINE_MIPSFPU16',
    0x1f0: 'IMAGE_FILE_MACHINE_POWERPC',
    0x1f1: 'IMAGE_FILE_MACHINE_POWERPCFP',
    0x166: 'IMAGE_FILE_MACHINE_R4000',
    0x5032: 'IMAGE_FILE_MACHINE_RISCV32',
    0x5064: 'IMAGE_FILE_MACHINE_RISCV64',
    0x5128: 'IMAGE_FILE_MACHINE_RISCV128',
    0x1a2: 'IMAGE_FILE_MACHINE_SH3',
    0x1a3: 'IMAGE_FILE_MACHINE_SH3DSP',
    0x1a6: 'IMAGE_FILE_MACHINE_SH4',
    0x1a8: 'IMAGE_FILE_MACHINE_SH5',
    0x1c2: 'IMAGE_FILE_MACHINE_THUMB',
    0x169: 'IMAGE_FILE_MACHINE_WCEMIPSV2'
    }

DLL_CHARACTERISTICS = {
    0x0001: 'Reserved, must be zero.',
    0x0002: 'Reserved, must be zero.',
    0x0004: 'Reserved, must be zero.',
    0x0008: 'Reserved, must be zero.',
    0x0020: 'IMAGE_DLLCHARACTERISTICS_HIGH_ENTROPY_VA',
    0x0040: 'IMAGE_DLLCHARACTERISTICS_DYNAMIC_BASE',
    0x0080: 'IMAGE_DLLCHARACTERISTICS_FORCE_INTEGRITY',
    0x0100: 'IMAGE_DLLCHARACTERISTICS_NX_COMPAT',
    0x0200: 'IMAGE_DLLCHARACTERISTICS_NO_ISOLATION',
    0x0400: 'IMAGE_DLLCHARACTERISTICS_NO_SEH',
    0x0800: 'IMAGE_DLLCHARACTERISTICS_NO_BIND',
    0x1000: 'IMAGE_DLLCHARACTERISTICS_APPCONTAINER',
    0x2000: 'IMAGE_DLLCHARACTERISTICS_WDM_DRIVER',
    0x4000: 'IMAGE_DLLCHARACTERISTICS_GUARD_CF',
    0x8000: 'IMAGE_DLLCHARACTERISTICS_TERMINAL_SERVER_AWARE'
}


class Header:
    def __init__(self, dump):
        self.dump = dump

    def extract_int(self, left, right) -> int:
        result = struct.unpack('I', self.dump[left:right])[0]
        return result

    def extract_short(self, left, right) -> int:
        result = struct.unpack('H', self.dump[left:right])[0]
        return result

    def extract_long(self, left, right) -> int:
        result = struct.unpack('Q', self.dump[left:right])[0]
        return result

    def extract_char(self, left, right):
        result = struct.unpack('b', self.dump[left:right])[0]
        return result


class DOSHeader(Header):

    def __init__(self, dump):
        super().__init__(dump)

    def dos_header(self):
        dh_tuple = struct.unpack('HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHI', self.dump[0:64])
        dos_h = collections.namedtuple('DOS_header', ['e_magic', 'e_cblp', 'e_cp', 'e_crlc', 'e_cparhdr',
                                                      'e_minalloc', 'e_maxalloc', 'e_ss', 'e_sp', 'e_csum',
                                                      'e_ip', 'e_cs', 'e_lfarlc', 'e_ovno', 'reserved11',
                                                      'reserved12', 'reserved13', 'reserved14', 'e_oemid',
                                                      'e_oeminfo', 'reserved21', 'reserved22', 'reserved23',
                                                      'reserved24', 'reserved25', 'reserved26', 'reserved27',
                                                      'reserved28', 'reserved29', 'reserved30', 'e_lfanew'])
        dh = dos_h(*dh_tuple)
        return dh

    def signature(self) -> str:
        """
        :return: Magic number
        """
        return '{}{}'.format(chr(self.dump[0]), chr(self.dump[1]))

    def e_cblp(self):
        """
        :return: bytes of last page
        """
        return self.extract_short(2, 4)

    def e_cp(self):
        """
        :return: pages in file
        """
        return self.extract_short(4, 6)

    def e_crlc(self):
        """
        :return: number of relocations
        """
        return self.extract_short(6, 8)

    def e_cparhdr(self):
        """
        :return: msdos header size(in paragraphs)
        """
        return self.extract_short(8, 10)

    def e_minalloc(self):
        """
        :return: minimum paragraphs
        """
        return self.extract_short(10, 12)

    def e_maxalloc(self):
        """
        :return: maximum paragraphs
        """
        return self.extract_short(12, 14)

    def e_ss(self):
        """
        :return: stack-segment module
        """
        return self.extract_short(14, 16)

    def e_sp(self):
        """
        :return: SP register
        """
        return self.extract_short(16, 18)

    def e_csum(self):
        """
        :return: checksum
        """
        return self.extract_short(18, 20)

    def e_ip(self):
        """
        :return: IP register
        """
        return self.extract_short(20, 22)

    def e_cs(self):
        """
        :return: Initial (relative) CS value
        """
        return self.extract_short(22, 24)

    def e_lfarlc(self):
        """
        :return: File address of relocation table
        """
        return self.extract_short(24, 26)

    def e_ovno(self):
        """
        :return: Overlay number
        """
        return self.extract_short(26, 28)

    def e_oemid(self):
        """
        :return: OEM identifier (for e_oeminfo)
        """
        return self.extract_short(32, 34)

    def e_oeminfo(self):
        """
        :return: OEM information; e_oemid specific
        """
        return self.extract_short(32, 34)

    def pe_offset(self) -> int:
        """
        :return: pe offset (in bytes)
        """
        return self.extract_int(60, 64)


class PEHeader(Header):

    def __init__(self, dump: bytes, offset: int):
        super().__init__(dump)
        self.offset = offset

    def machine(self):
        """
        :return: The number that identifies the type of target machine.
        """
        mchn = self.extract_short(self.offset + 4, self.offset + 6)
        return MACHINE_TYPES[mchn]

    def number_of_sections(self) -> int:
        """
        The number of sections.
        This indicates the size of the section table, which immediately follows the headers.
        :return:
        number of sections (int)
        """
        return self.extract_short(self.offset + 6, self.offset + 8)

    def time_date_stamp(self) -> datetime:
        """
        The low 32 bits of the number of seconds since 00:00 January 1, 1970 (a C run-time time_t value),
        that indicates when the file was created.
        :return:
        datetime object containing date and time of creation
        """
        dt = self.extract_int(self.offset + 8, self.offset + 12)
        return datetime.utcfromtimestamp(dt)

    def pointer_to_symbol_table(self):
        """
        :return: The file offset of the COFF symbol table, or zero if no COFF symbol table is present.
        """
        return self.extract_int(self.offset + 12, self.offset + 16)

    def number_of_symbols(self):
        """
        :return: The number of entries in the symbol table.
        """
        return self.extract_int(self.offset + 16, self.offset + 20)

    def size_of_optional_header(self) -> int:
        """
        :return: The size of the optional header, which is required for executable files but not for object files.
        """
        return self.extract_short(self.offset + 20, self.offset + 22)

    def characteristics(self):
        """
        The flags that indicate the attributes of the file.
        :return:characteristics
        """
        result = []
        chrct = self.dump[self.offset + 22: self.offset + 24]
        unpacked = struct.unpack('H', chrct)[0]
        for mask in CHARACTERISTIC_MASKS.keys():
            if unpacked & mask > 0:
                result.append(CHARACTERISTIC_MASKS[mask])
        return result


class OptionalHeader(Header):
    def __init__(self, dump: bytes, optional_offset: int):
        super().__init__(dump)
        self.optional_offset = optional_offset

    def magic(self) -> str:
        """
        The unsigned integer that identifies the state of the image file.
        The most common number is 0x10B, which identifies it as a normal executable file.
        0x107 identifies it as a ROM image, and 0x20B identifies it as a PE32+ executable.
        :return:The optional header magic number determines whether an image is a PE32 or PE32+ executable.
        """
        mgck = self.dump[self.optional_offset: self.optional_offset + 2]
        if mgck == b'\x0b\x01':
            return 'x32'
        elif mgck == b'\x0b\x02':
            return 'x64'
        elif mgck == b'\x07\x01':
            return 'ROM'
        else:
            raise ValueError('Unknown image')

    def major_linker_version(self):
        """
        :return: The linker major version number.
        """
        return self.extract_char(self.optional_offset + 2, self.optional_offset + 3)

    def minor_linker_version(self):
        """
        :return: The linker minor version number.
        """
        return self.extract_char(self.optional_offset + 3, self.optional_offset + 4)

    def size_of_code(self):
        """
        :return:The size of the code (text) section, or the sum of all code sections if there are multiple sections.
        """
        return self.extract_int(self.optional_offset + 4, self.optional_offset + 8)

    def size_of_initialized_data(self):
        """
        :return:The size of the initialized data section,
        or the sum of all such sections if there are multiple data sections.
        """
        return self.extract_int(self.optional_offset + 8, self.optional_offset + 12)

    def size_of_uninitialized_data(self):
        """
        :return:The size of the uninitialized data section (BSS),
        or the sum of all such sections if there are multiple BSS sections.
        """
        return self.extract_int(self.optional_offset + 12, self.optional_offset + 16)

    def address_of_entry_point(self) -> str:
        """
        :return:
        The address of the entry point relative to the image base when the executable file is loaded into memory.
        """
        return hex(self.extract_int(self.optional_offset + 16, self.optional_offset + 20))

    def base_of_code(self) -> str:
        """
        :return:
        The address that is relative to the image base of the beginning-of-code section when it is loaded into memory.
        """
        return hex(self.extract_int(self.optional_offset + 20, self.optional_offset + 24))

    def base_of_data(self) -> str:
        """
        :return:
        The address that is relative to the image base of the beginning-of-data section when it is loaded into memory.
        """
        return hex(self.extract_short(self.optional_offset + 24, self.optional_offset + 26))

    def image_base(self) -> str:
        """
        :return: The preferred address of the first byte of image when loaded into memory
        """
        if self.magic() == 'x32':
            return hex(self.extract_int(self.optional_offset + 28, self.optional_offset + 32))
        else:
            return hex(self.extract_long(self.optional_offset + 24, self.optional_offset + 32))

    def section_alignment(self) -> str:
        """
        :return: The alignment (in bytes) of sections when they are loaded into memory.
        """
        return hex(self.extract_int(self.optional_offset + 32, self.optional_offset + 36))

    def file_alignment(self) -> str:
        """
        :return:The alignment factor (in bytes) that is used to align the raw data of sections in the image file.
        """
        return hex(self.extract_int(self.optional_offset + 36, self.optional_offset + 40))

    def major_operating_system_version(self):
        """
        :return: The major version number of the required operating system.
        """
        return self.extract_short(self.optional_offset + 40, self.optional_offset + 42)

    def minor_operating_system_version(self):
        """
        :return: The minor version number of the required operating system.
        """
        return self.extract_short(self.optional_offset + 42, self.optional_offset + 44)

    def major_image_version(self):
        """
        :return: The major version number of the image.
        """
        return self.extract_short(self.optional_offset + 44, self.optional_offset + 46)

    def minor_image_version(self):
        """
        :return: The minor version number of the image.
        """
        return self.extract_short(self.optional_offset + 46, self.optional_offset + 48)

    def major_subsystem_version(self):
        """
        :return: The major version number of the image.
        """
        return self.extract_short(self.optional_offset + 48, self.optional_offset + 50)

    def minor_subsystem_version(self):
        """
        :return: The minor version number of the image.
        """
        return self.extract_short(self.optional_offset + 50, self.optional_offset + 52)

    def size_of_image(self) -> str:
        """
        :return: The size (in bytes) of the image, including all headers, as the image is loaded in memory.
        """
        return hex(self.extract_int(self.optional_offset + 56, self.optional_offset + 60))

    def size_of_headers(self) -> str:
        """
        :return: The size (in bytes) of the image, including all headers, as the image is loaded in memory.
        """
        return hex(self.extract_int(self.optional_offset + 60, self.optional_offset + 64))

    def checksum(self) -> str:
        """
        :return: The image file checksum
        """
        return hex(self.extract_int(self.optional_offset + 64, self.optional_offset + 68))

    def subsystem(self):
        """
        :return: The subsystem that is required to run this image.
        """
        subsystem = self.extract_short(self.optional_offset + 68, self.optional_offset + 70)
        return SUBSYSTEM_VALUE[subsystem]

    def dll_characteristics(self):
        """
        :return: DLL Characteristics
        """
        dll_chrct = self.extract_short(self.optional_offset + 70, self.optional_offset + 72)
        return DLL_CHARACTERISTICS[dll_chrct]

    def size_of_stack_reserve(self):
        """
        :return: The size of the stack to reserve.
        """
        if self.magic() == 'x32':
            return hex(self.extract_int(self.optional_offset + 72, self.optional_offset + 76))
        else:
            return hex(self.extract_long(self.optional_offset + 72, self.optional_offset + 80))

    def size_of_stack_commit(self):
        """
        :return: The size of the stack to commit.
        """
        if self.magic() == 'x32':
            return hex(self.extract_int(self.optional_offset + 76, self.optional_offset + 80))
        else:
            return hex(self.extract_long(self.optional_offset + 80, self.optional_offset + 88))

    def size_of_heap_reserve(self):
        """
        :return: The size of the local heap space to reserve.
        """
        if self.magic() == 'x32':
            return hex(self.extract_int(self.optional_offset + 80, self.optional_offset + 84))
        else:
            return hex(self.extract_long(self.optional_offset + 88, self.optional_offset + 92))

    def size_of_heap_commit(self):
        """
        :return: The size of the local heap space to commit.
        """
        if self.magic() == 'x32':
            return hex(self.extract_int(self.optional_offset + 80, self.optional_offset + 84))
        else:
            return hex(self.extract_long(self.optional_offset + 88, self.optional_offset + 96))

    def number_of_rva_and_sizes(self):
        """
        :return: The number of data-directory entries in the remainder of the optional header.
        Each describes a location and size.
        """
        if self.magic() == 'x32':
            return self.extract_int(self.optional_offset + 92, self.optional_offset + 96)
        else:
            return self.extract_int(self.optional_offset + 108, self.optional_offset + 112)


class DataDirectory(Header):
    def __init__(self, dump: bytes, optional_offset: int, number_of_rva_sizes: int, magic: str):
        super().__init__(dump)
        self.optional_offset = optional_offset
        self.number_of_rva_sizes = number_of_rva_sizes
        self.magic = magic

    def image_data_directory(self):
        """
        :return: Each data directory gives the address and size of a table or string that Windows uses.
        """
        dir = []
        idx = 0
        offset = 96 if self.magic == 'x32' else 112
        while idx < self.number_of_rva_sizes:
            img_data_directory = {
                'virtual_address': hex(
                    self.extract_int(self.optional_offset + offset, self.optional_offset + offset + 4)),
                'size': hex(
                    self.extract_int(self.optional_offset + offset + 4, self.optional_offset + offset + 8))
            }
            dir.append(img_data_directory)
            offset = offset + 8
            idx = idx + 1
        return {'ExportTable': dir[0],
                'ImportTable': dir[1],
                'Resource': dir[2],
                'Exception': dir[3],
                'Security': dir[4],
                'Relocations': dir[5],
                'Debug': dir[6],
                'CopyRight': dir[7],
                'Globalptr': dir[8],
                'TlsTable': dir[9],
                'LoadConfig': dir[10],
                'BoundImport': dir[11],
                'IAT': dir[12],
                'DelayImport': dir[13],
                'COM': dir[14],
                'Reserved': dir[15]}


class SectionTable(Header):
    def __init__(self, dump: bytes, magic: str, optional_offset: int, number_of_sections: int):
        super().__init__(dump)
        self.magic = magic
        self.optional_offset = optional_offset
        self.number_of_sections = number_of_sections
        self.st_offset = 216 if self.magic == 'x32' else 232
        self.section_table_offset = self.optional_offset + self.st_offset + 8

    def section_table(self):
        """
        :return: Sections header
        """
        idx = 0
        st_list = list()
        while idx < self.number_of_sections:
            t_size = idx * 40  # summary size of section table field's types
            st_dict = {
                'name': str(utils.ByteStr(
                    self.dump[self.section_table_offset + t_size:self.section_table_offset + 8 + t_size])),
                'virtual_size': hex(self.extract_int(self.section_table_offset + 8 + t_size,
                                                     self.section_table_offset + 12 + t_size)),
                'virtual_address': hex(self.extract_int(self.section_table_offset + 12 + t_size,
                                                        self.section_table_offset + 16 + t_size)),
                'size_of_raw_data': hex(self.extract_int(self.section_table_offset + 16 + t_size,
                                                         self.section_table_offset + 20 + t_size)),
                'pointer_to_raw_data': hex(self.extract_int(self.section_table_offset + 20 + t_size,
                                                            self.section_table_offset + 24 + t_size)),
                'pointer_to_relocations': hex(self.extract_int(self.section_table_offset + 24 + t_size,
                                                               self.section_table_offset + 28 + t_size)),
                'pointer_to_linenumbers': hex(self.extract_int(self.section_table_offset + 28 + t_size,
                                                               self.section_table_offset + 32 + t_size)),
                'number_of_relocations': hex(self.extract_short(self.section_table_offset + 32 + t_size,
                                                                self.section_table_offset + 34 + t_size)),
                'number_of_linenumbers': hex(self.extract_short(self.section_table_offset + 34 + t_size,
                                                                self.section_table_offset + 36 + t_size)),
                'characteristics': hex(self.extract_int(self.section_table_offset + 36 + t_size,
                                                        self.section_table_offset + 40 + t_size))
            }

            st_list.append(st_dict)
            idx = idx + 1
        return st_list





def is_valid_dos_header(header: DOSHeader) -> bool:
    return header.signature() == 'MZ'

